package nl.utwente.di.bookQuote;

public class Quoter {

    public double getBookPrice(String isbn){

        if (Integer. parseInt(isbn) == 1){
            return 10.0;
        }
        if (Integer. parseInt(isbn) == 2){
            return 45.0;
        }
        if (Integer. parseInt(isbn) == 3){
            return 20.0;
        }
        if (Integer. parseInt(isbn) == 4){
            return 35.0;
        }
        if (Integer. parseInt(isbn) == 5){
            return 50.0;
        }
        else return 0;
    }
}